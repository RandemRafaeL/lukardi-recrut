import { Injectable } from '@angular/core'
import {environment} from '../../environments/environment'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Post, PostDetail} from '../interfaces/posts'
import {CacheRegisterService} from '../cache/cacheRegister.service'

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = environment.apiUrl

  constructor(
    private http: HttpClient,
    private cacheRegister: CacheRegisterService
  ) {

  }


  public getAllPosts( resultStartNumber: number = 2 , resultsNumber: number = 10,  ): Observable<Post[]>{

    const params = {
      offset: resultStartNumber.toString(),
      number: resultsNumber.toString() || '5',    // number of post
      fields: 'ID,title,featured_image,excerpt,author,name,slug,found,date'
    }

    this.cacheRegister.addToCache(`${this.apiUrl}/posts`)

    return this.http
      .get<Post[]>(`${this.apiUrl}/posts`, { params: {...params } } )
  }



  getCommentsByPostID(id: number){

    const params = {
      hierarchical: 'true'
    }

    return this.http
      .get<Post[]>(`${this.apiUrl}/posts/${id}/replies/`, { params: {...params } } )
  }


  public getOnePostBySlug(slug: string) {
    const params = {
      // fields: 'ID,title,featured_image,excerpt,author,name,slug,found,content,date'
    }
    this.cacheRegister.addToCache(`${this.apiUrl}/posts/slug:${slug}`)

    return this.http.get<PostDetail>(`${this.apiUrl}/posts/slug:${slug}`, {params: {...params}} )
  }
}
