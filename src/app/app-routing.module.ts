import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'


const routes: Routes = [
  {path: 'blog', loadChildren: () => import('./pages/recruitment-task/recruitment-task.module').then( mod => mod.RecruitmentTaskModule) },
  {path: '**' , redirectTo: 'blog' , pathMatch: 'full'}
  ]

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false,
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'disabled',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
