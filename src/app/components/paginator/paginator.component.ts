import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core'
import {PaginatorInput} from '../../interfaces/paginator'

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.sass']
})
export class PaginatorComponent implements OnChanges {

  @Input() state: PaginatorInput
  @Output() _currentPage = new EventEmitter()

  currentPage = 1
  numberPages: number
  pageNames: any[] = []

  paginatorSize = 8
  paginatorStartPage = 1

  prevButton: number = null
  nextButton: number = null

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initPaginator()
  }

  nextPage(){
    if (this.currentPage < this.numberPages) {  this.setPage(this.currentPage + 1) }
    if (this.currentPage > this.paginatorStartPage + this.paginatorSize - 1) {
      this.nextPaginator()
    }
  }

  prevPage(){
    if (this.currentPage > 1 ) { this.setPage(this.currentPage - 1) }
    if (this.currentPage < this.paginatorStartPage ) {
      this.prevPaginator()
    }
  }


  initNextPrevButtons(){
    this.prevButton = this.paginatorStartPage - this.paginatorSize
    this.nextButton = this.paginatorStartPage + this.paginatorSize
  }

  setPage(value){
    this.currentPage = value
    this._currentPage.emit(this.currentPage * this.state.pageSize - this.state.pageSize + 1)
  }

  nextPaginator(){
    if ( this.paginatorStartPage < this.numberPages - this.paginatorSize) {
      this.paginatorStartPage += this.paginatorSize
    }
    this.initNextPrevButtons()
  }

  prevPaginator(){
    if ( this.paginatorStartPage  > 0 ) {
      this.paginatorStartPage -= this.paginatorSize
      if (this.paginatorStartPage < this.paginatorSize) { this.paginatorStartPage = 1 }
    }
    this.initNextPrevButtons()
  }

  buttonPrevNextMany(value: number) {
    this.paginatorStartPage = value
    this.initNextPrevButtons()
  }

  initPaginator(){
    this.numberPages = Math.round(this.state.numberResults / this.state.pageSize)
    this.currentPage = Math.round(((this.state.firstPostOnPage /  this.state.pageSize)) ) + 1

    this.paginatorStartPage = +this.currentPage - this.paginatorSize / 2
    if ( this.paginatorStartPage < 1 ) { this.paginatorStartPage = 1 }

    this.pageNames.length = this.numberPages

    for ( let index = 0; index < this.numberPages ; index ++) {
      this.pageNames[index] = index + 1
    }

    this.initNextPrevButtons()
  }
}

