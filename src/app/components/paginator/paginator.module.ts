import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import {PaginatorComponent} from './paginator.component'
import {FlexLayoutModule} from '@angular/flex-layout'



@NgModule({
  declarations: [PaginatorComponent],
  exports: [
    PaginatorComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule
  ]
})
export class PaginatorModule { }
