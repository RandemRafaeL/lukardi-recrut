import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes} from '@angular/router'
import { MatToolbarModule } from '@angular/material/toolbar'
import { FlexLayoutModule } from '@angular/flex-layout'
import { PostsAllComponent } from './posts-all/posts-all.component'
import { PostsOneComponent } from './posts-one/posts-one.component'
import { PaginatorModule} from '../../components/paginator/paginator.module'

const routes: Routes = [
  {path: 'post/:id', component: PostsOneComponent},
  {path: 'posts', component: PostsAllComponent},
  {path: 'posts/:id' , redirectTo: 'posts' },
  {path: '**' , redirectTo: 'posts' }
]

@NgModule({
  declarations: [PostsAllComponent, PostsOneComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatToolbarModule,
    FlexLayoutModule,
    PaginatorModule
  ]
})
export class RecruitmentTaskModule { }
