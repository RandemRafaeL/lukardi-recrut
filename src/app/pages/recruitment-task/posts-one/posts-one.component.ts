import {Component, OnInit, ViewEncapsulation} from '@angular/core'
import {PostsService} from '../../../services/posts.service'
import { Observable } from 'rxjs'
import {ActivatedRoute} from '@angular/router'
import {map, shareReplay, switchMap, tap} from 'rxjs/operators'
import {CommentsForPost, PostDetail} from '../../../interfaces/posts'

@Component({
  selector: 'app-posts-one',
  templateUrl: './posts-one.component.html',
  styleUrls: ['./posts-one.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class PostsOneComponent implements OnInit {

  post$: Observable<PostDetail>
  comments$: Observable<CommentsForPost[]>

  constructor(
    private postApiService: PostsService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const slug = this.activatedRoute.snapshot.params.id
    console.log(slug)

    this.post$ = this.postApiService
      .getOnePostBySlug(slug)
      .pipe(shareReplay())

    this.comments$ = this.post$
      .pipe(
        tap( el => console.log(el)),
        map(post => post.ID),
        switchMap( (id: any) => this.postApiService.getCommentsByPostID(id)),
        shareReplay(1),
        map( (data: any) => this.parseComments(data.comments)),
      )

  }

  parseComments(comments: CommentsForPost[]){
    let _comments: CommentsForPost[]
    let _commentsChildren: CommentsForPost[]

    _comments = comments
      .filter( el => !el.parent)
      .map( el => ({...el, treeLevel: 0}))

    _commentsChildren = comments
      .filter( el => el.parent)

    _commentsChildren.forEach( child => {
      const indexParent = _comments.map(el => el.ID).indexOf(child.parent.ID)
      const parent = _comments[indexParent]
      _comments.splice( indexParent + 1 , 0, {...child, treeLevel: parent.treeLevel + 1})
    })

    return _comments
  }


}

