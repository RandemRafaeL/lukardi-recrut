import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PostsOneComponent } from './posts-one.component'

describe('PostsOneComponent', () => {
  let component: PostsOneComponent
  let fixture: ComponentFixture<PostsOneComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsOneComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsOneComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
