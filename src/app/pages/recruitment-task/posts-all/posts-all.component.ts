import {Component, OnInit, ViewEncapsulation} from '@angular/core'
import {PostsService} from '../../../services/posts.service'
import {filter, map, shareReplay, tap} from 'rxjs/operators'
import {Observable} from 'rxjs'
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router'
import {Post, PostsQueryParams} from '../../../interfaces/posts'
import {PaginatorInput} from '../../../interfaces/paginator'

@Component({
  selector: 'app-posts-all',
  templateUrl: './posts-all.component.html',
  styleUrls: ['./posts-all.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class PostsAllComponent implements OnInit {

  posts$: Observable<Post[]>

  paginatorInput: PaginatorInput

  queryParams: PostsQueryParams
  initQueryParams: PostsQueryParams = {
    firstPostOnPage: 1,
    postsOnPage: 10,
  }

  constructor(
    private postApiService: PostsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.queryParams = {...this.initQueryParams}
  }

  ngOnInit(): void {

    if ( this.activatedRoute.snapshot.queryParams ){
      this.queryParams = {...this.queryParams, ...this.activatedRoute.snapshot.queryParams} }
    if (this.activatedRoute.snapshot.queryParams.postsOnPage > 100) {
      this.queryParams.postsOnPage = 100 }
    if (this.activatedRoute.snapshot.queryParams.postsOnPage < 4 ) {
      this.queryParams.postsOnPage = 4 }

    this.getPosts()


    this.router.events
      .pipe(
        filter( event => event instanceof NavigationEnd )
      )
      .subscribe(() => {
        this.queryParams = {...this.queryParams, ...this.activatedRoute.snapshot.queryParams}
        this.getPosts()
      })

  }




  getPosts(){

    this.posts$ = this.postApiService
      .getAllPosts(this.queryParams.firstPostOnPage, this.queryParams.postsOnPage)
      .pipe(
        tap((data: any) => {
          if (!data.found){
            this.router.navigate(['/blog/posts'] , {queryParams: {...this.initQueryParams}}).then()
          }
          this.paginatorInput = {
            numberResults: data.found,
            pageSize: this.queryParams.postsOnPage,
            firstPostOnPage: this.queryParams.firstPostOnPage < data.found ? this.queryParams.firstPostOnPage : data.found
          }
        }),
        shareReplay(),
        map((data: any) => data.posts ) )
  }


  getPostsByStartNumber(resultStartNumber){
    this.queryParams = {...this.queryParams, firstPostOnPage: resultStartNumber}
    this.router
      .navigate(['/blog/posts'] , {queryParams: {...this.queryParams}} )
      .then()
  }



}

