import { Injectable } from '@angular/core'

@Injectable(
  { providedIn: 'root'}
)
export class CacheRegisterService {
  private services = []

  public isAddedToCache(serviceUri: string) {
    return this.services.indexOf(serviceUri) > -1
  }

  public addToCache(serviceUri: string) {

    if (!this.isAddedToCache(serviceUri)) {
      this.services.push(serviceUri)
    }
  }
}
