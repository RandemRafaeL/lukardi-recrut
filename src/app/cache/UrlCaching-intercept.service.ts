import { Injectable } from '@angular/core'
import {HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http'

import {Observable, of} from 'rxjs'
import {CacheRegisterService } from './cacheRegister.service'
import {share, tap} from 'rxjs/operators'

@Injectable(
  { providedIn: 'root' }
)
export class UrlCacheInterceptor implements HttpInterceptor {
  private cachedData = new Map<string, any>()

  constructor(private cacheRegistrationService: CacheRegisterService) {
  }

  public intercept(httpRequest: HttpRequest<any>, handler: HttpHandler) {

    if (httpRequest.method !== 'GET' ||
      !this.cacheRegistrationService.isAddedToCache(httpRequest.url)) {
      return handler.handle(httpRequest)
    }

    if (httpRequest.headers.get('reset-cache')) {
      this.cachedData.delete(httpRequest.urlWithParams)
    }

    const lastResponse = this.cachedData.get(httpRequest.urlWithParams)
    if (lastResponse) {
      return (lastResponse instanceof Observable)
        ? lastResponse : of(lastResponse.clone())
    }

    const requestHandle = handler.handle(httpRequest)
      .pipe(
        tap((stateEvent) => {
          if (stateEvent instanceof HttpResponse) {
            this.cachedData.set(
              httpRequest.urlWithParams,
              stateEvent.clone()
            )
          }
        }),
        share()
      )

    this.cachedData.set(httpRequest.urlWithParams, requestHandle)

    return requestHandle
  }
}
