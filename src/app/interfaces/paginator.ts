export interface PaginatorInput {
  numberResults: number
  pageSize: number
  firstPostOnPage: number
}
