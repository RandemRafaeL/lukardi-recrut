export interface PostsQueryParams {
  postsOnPage: number
  firstPostOnPage: number
}

export interface Post {
  ID: number
  title: string
  featured_image: string
  excerpt: string
  date: string
  author: {
    name: string
  }
  slug: string

  found: number
}

export interface PostDetail extends Post{
  content: string
}

export interface CommentsForPost {
  ID: number
  author: {
    name: string
    avatar_URL: string
  }
  parent: {
    ID: number
  } | any

  content: string
  date: string

  treeLevel?: number
}
