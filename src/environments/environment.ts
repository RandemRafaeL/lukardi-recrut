export const environment = {
  production: false,
  apiUrl: 'https://public-api.wordpress.com/rest/v1.1/sites/en.blog.wordpress.com'
}
